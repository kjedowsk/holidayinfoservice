package com.holidayinfo.http;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class HttpClient {

    private final RestTemplate restTemplate;

    public HttpClient() {
        restTemplate = new RestTemplate();
    }

    public <T> T doGET(String uri, Map<String, String> params, Class<T> cls) {
        ResponseEntity<T> response = restTemplate.getForEntity(uri, cls, params);
        return response.getBody();
    }

}
