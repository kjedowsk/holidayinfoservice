package com.holidayinfo.model.types;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SharedDateHolidays {

    private String date;

    private String name1;

    private String name2;
}
