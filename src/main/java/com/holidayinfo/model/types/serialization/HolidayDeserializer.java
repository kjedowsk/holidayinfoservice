package com.holidayinfo.model.types.serialization;

import com.holidayinfo.model.types.Holiday;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.time.LocalDate;

public class HolidayDeserializer extends JsonDeserializer<Holiday> {

    @Override
    public Holiday deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String name = node.get("name").asText();
        String date = node.get("date").asText();
        String observed = node.get("observed").asText();
        boolean isPublic = node.get("public").asBoolean();
        return new Holiday(name, LocalDate.parse(date), LocalDate.parse(observed), isPublic);
    }
}
