package com.holidayinfo.model.types;

import com.holidayinfo.model.types.serialization.HolidayDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
@JsonDeserialize(using = HolidayDeserializer.class)
public class Holiday {

    private String name;

    private LocalDate date;

    private LocalDate observed;

    private boolean isPublic;
}
