package com.holidayinfo.model.responses;

import com.holidayinfo.model.types.Holiday;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@NoArgsConstructor
@Setter
@Getter
public class HolidayApiResponse {

    private int status;

    private ArrayList<Holiday> holidays;
}
