package com.holidayinfo.controllers;

import com.holidayinfo.exceptions.HolidayApiException;
import com.holidayinfo.model.types.SharedDateHolidays;
import com.holidayinfo.services.HolidayInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class HolidayInfoController {

    private final HolidayInfoService holidayInfoService;

    @Autowired
    public HolidayInfoController(HolidayInfoService holidayInfoService) {
        this.holidayInfoService = holidayInfoService;
    }

    @GetMapping("/sharedDateHolidays")
    public SharedDateHolidays sharedDateHolidays(@RequestParam(value = "countryCode1") String code1,
                                                 @RequestParam(value = "countryCode2") String code2,
                                                 @RequestParam(value = "date") String date) throws HolidayApiException {
        return holidayInfoService.getSharedDateHolidays(code1, code2, date);
    }
}
