package com.holidayinfo.exceptions;

public class HolidayApiException extends Exception {

    public HolidayApiException(String msg) {
        super(msg);
    }
}
