package com.holidayinfo.services;

import com.holidayinfo.exceptions.HolidayApiException;
import com.holidayinfo.model.types.SharedDateHolidays;
import com.holidayinfo.model.types.Holiday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class HolidayInfoService {

    private static final int DAYS_TO_ADD = 1;

    private final HolidayApiService holidayApiService;

    @Autowired
    public HolidayInfoService(HolidayApiService holidayApiService) {
        this.holidayApiService = holidayApiService;
    }

    public SharedDateHolidays getSharedDateHolidays(String countryCode1, String countryCode2, String date) throws HolidayApiException {
        LocalDate startingDate = LocalDate.parse(date);
        return getSharedDateHolidays(countryCode1, countryCode2, startingDate, startingDate);
    }

    private SharedDateHolidays getSharedDateHolidays(String countryCode1, String countryCode2, LocalDate startingDate1, LocalDate startingDate2) throws HolidayApiException {
        Holiday holiday1 = holidayApiService.getSingleUpcomingHoliday(countryCode1, startingDate1);
        Holiday holiday2 = holidayApiService.getSingleUpcomingHoliday(countryCode2, startingDate2);
        LocalDate upcomingDate1 = holiday1.getDate();
        LocalDate upcomingDate2 = holiday2.getDate();

        if (upcomingDate1.isEqual(upcomingDate2))
            return new SharedDateHolidays(upcomingDate1.toString(), holiday1.getName(), holiday2.getName());
        if (upcomingDate1.isBefore(upcomingDate2))
            return getSharedDateHolidays(countryCode1, countryCode2, upcomingDate1.plusDays(DAYS_TO_ADD), upcomingDate2);
        else
            return getSharedDateHolidays(countryCode1, countryCode2, upcomingDate1, upcomingDate2.plusDays(DAYS_TO_ADD));
    }
}
