package com.holidayinfo.services;

import com.holidayinfo.conf.HolidayApiConf;
import com.holidayinfo.exceptions.HolidayApiException;
import com.holidayinfo.http.HttpClient;
import com.holidayinfo.model.responses.HolidayApiResponse;
import com.holidayinfo.model.types.Holiday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

@Service
public class HolidayApiService {

    private final static String HOLIDAY_API_ENDPOINT = "/v1/holidays";
    private final static String QUERY_PARAMS = "?key={key}&country={country}&year={year}&month={month}&day={day}&upcoming=true";

    private final HolidayApiConf holidayApiConf;

    private final HttpClient httpClient;

    @Autowired
    public HolidayApiService(HolidayApiConf holidayApiConf, HttpClient httpClient) {
        this.holidayApiConf = holidayApiConf;
        this.httpClient = httpClient;
    }

    Holiday getSingleUpcomingHoliday(String country, LocalDate date) throws HolidayApiException {
        return getUpcomingHolidays(country, date).get(0);
    }

    private ArrayList<Holiday> getUpcomingHolidays(String country, LocalDate date) throws HolidayApiException {
        HolidayApiResponse response = httpClient.doGET(getHolidayApiUri(), getParams(country, date), HolidayApiResponse.class);
        if (response.getHolidays().isEmpty())
            throw new HolidayApiException("HolidayAPI service didn't return any upcoming holidays.");
        return response.getHolidays();
    }

    private HashMap<String, String> getParams(String country, LocalDate date) {
        HashMap<String, String> params = new HashMap<>();
        params.put("key", holidayApiConf.getApiKey());
        params.put("country", country);
        params.put("year", String.valueOf(date.getYear()));
        params.put("month", String.valueOf(date.getMonthValue()));
        params.put("day", String.valueOf(date.getDayOfMonth()));
        return params;
    }

    private String getHolidayApiUri() {
        return holidayApiConf.getAddr() + HOLIDAY_API_ENDPOINT + QUERY_PARAMS;
    }
}
